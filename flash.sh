#!/bin/bash

set -e

dfu-programmer atmega32u4 erase --force
dfu-programmer atmega32u4 flash --force "${1}"
dfu-programmer atmega32u4 reset
