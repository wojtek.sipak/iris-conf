https://config.qmk.fm/#/keebio/iris/rev6/LAYOUT

https://docs.qmk.fm/#/flashing

    dfu-programmer atmega32u4 erase --force
    dfu-programmer atmega32u4 flash --force keebio_iris_rev6_iriskb.hex
    dfu-programmer atmega32u4 reset
